"use strict";

const request = require("supertest");
const expect = require("chai").expect;

let app = require("../app");

describe("Rides API tests", () => {
    describe("GET empty /rides", () => {
        it("should return empty array", (done) => {
            request(app)
                .get("/rides")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array").that.is.empty;
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET empty /rides/1", () => {
        it("should return not found error message", (done) => {
            request(app)
                .get("/rides/1")
                .then((res) => {
                    expect(res.statusCode).to.equal(404);

                    const body = res.body;
                    expect(body).to.be.deep.equal({
                        error_code: "RIDES_NOT_FOUND_ERROR",
                        message: "Could not find any rides",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("POST /rides", () => {
        it("should create new ride", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(201);

                    const body = res.body;
                    expect(body).to.contain.property("created");
                    expect(body).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate start latitude", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 100,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message:
                            "Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate start longitude", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 200,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message:
                            "Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate end latitude", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 100,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message:
                            "End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate end longitude", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 200,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message:
                            "End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate rider name existence", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "",
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Rider name must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate rider name type", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: 200,
                    driver_name: "Driver Test",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Rider name must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate driver name existence", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "",
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Driver name must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate driver name type", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: 200,
                    driver_vehicle: "Vehicle Test",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Driver name must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate driver vehicle existence", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: "",
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Driver vehicle must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });

        it("should validate driver vehicle type", (done) => {
            request(app)
                .post("/rides")
                .set("content-type", "application/json")
                .send({
                    start_lat: 0,
                    start_long: 0,
                    end_lat: 0,
                    end_long: 0,
                    rider_name: "Rider Test",
                    driver_name: "Driver Test",
                    driver_vehicle: 200,
                })
                .then((res) => {
                    expect(res.statusCode).to.equal(400);

                    const body = res.body;
                    expect(body).to.deep.equal({
                        error_code: "VALIDATION_ERROR",
                        message: "Driver vehicle must be a non empty string",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET /rides", () => {
        it("should return data array with 10 max length", (done) => {
            request(app)
                .get("/rides")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array");
                    expect(body).to.have.lengthOf.below(11);
                    expect(body[0]).to.contain.property("created");
                    expect(body[0]).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET /rides?limit=1&page=1", () => {
        it("should return the first page data array with 1 max length", (done) => {
            request(app)
                .get("/rides?limit=1&page=1")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array");
                    expect(body).to.have.lengthOf(1);
                    expect(body[0]).to.contain.property("created");
                    expect(body[0]).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET /rides?limit=1&page=2", () => {
        it("should return the second page data array which is empty", (done) => {
            request(app)
                .get("/rides?limit=1&page=2")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array").that.is.empty;
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET /rides?limit=1", () => {
        it("should return data array with 1 max length", (done) => {
            request(app)
                .get("/rides?limit=1")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array");
                    expect(body).to.have.lengthOf(1);
                    expect(body[0]).to.contain.property("created");
                    expect(body[0]).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });

    describe("GET /rides?page=1", () => {
        it("should return first page data array with 10 max length", (done) => {
            request(app)
                .get("/rides?page=1")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.be.an("array");
                    expect(body).to.have.lengthOf.below(11);
                    expect(body[0]).to.contain.property("created");
                    expect(body[0]).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });
    
    describe("GET /rides/1", () => {
        it("should return single ride data", (done) => {
            request(app)
                .get("/rides/1")
                .then((res) => {
                    expect(res.statusCode).to.equal(200);

                    const body = res.body;
                    expect(body).to.contain.property("created");
                    expect(body).to.include({
                        rideID: 1,
                        startLat: 0,
                        startLong: 0,
                        endLat: 0,
                        endLong: 0,
                        riderName: "Rider Test",
                        driverName: "Driver Test",
                        driverVehicle: "Vehicle Test",
                    });
                    done();
                })
                .catch((err) => done(err));
        });
    });
});
