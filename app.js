"use strict";

const express = require("express");
const logger = require("./component/logger");

const app = express();

if (process.env.NODE_ENV !== "test") {
    logger.debug("Start logger");
    app.use(require("morgan")("combined", { stream: logger.stream }));
}

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

app.use(jsonParser);
require("./routes")(app);

module.exports = app;
