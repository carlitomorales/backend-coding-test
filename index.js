"use strict";

const port = 8010;
const app = require("./app");

app.listen(port, () =>
    console.log(`App started and listening on port ${port}`)
);
