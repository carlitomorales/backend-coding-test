"use strict";

const express = require("express");
const controller = require("./rides.controller");

var router = express.Router();

router.get("/", controller.index);
router.get("/:id", controller.show);
router.post("/", controller.store);

module.exports = router;
