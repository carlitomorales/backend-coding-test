"use strict";

const winston = require("winston");
const fs = require("fs");

const logDir = "logs";
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => new Date().toLocaleTimeString();

const logger = new winston.createLogger({
    transports: [
        new (require("winston-daily-rotate-file"))({
            filename: `${logDir}/%DATE%-results.log`,
            timestamp: tsFormat,
            datePattern: "yyyy-MM-DD",
            prepend: true,
            level: "info",
        }),
    ],
});

if (process.env.NODE_ENV !== "test") {
    logger.add(new winston.transports.Console, {
        timestamp: tsFormat,
        level: "debug",
        handleExceptions: true,
        json: false,
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
        ),
    });
}

module.exports = logger;
module.exports.stream = {
    write: function (message) {
        logger.info(message);
    },
};
