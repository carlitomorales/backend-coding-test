module.exports = (app) => {
    app.use("/health", require("./src/api/health"));
    app.use("/rides", require("./src/api/ride"));
    
    return app;
};