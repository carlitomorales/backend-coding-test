const logger = require("../../../component/logger");
const db = require("../../../database");

module.exports = {
    index: async (req, res) => {
        const skip =
            req.query.page && req.query.limit
                ? (req.query.page - 1) * req.query.limit
                : 0;
        const limit = req.query.limit ? parseInt(req.query.limit) : 10;
        const rows = await db.query("SELECT * FROM Rides limit ?, ?", [
            skip,
            limit,
        ]);
        res.status(200).send(rows);
    },

    show: async (req, res) => {
        const row = await db.get("SELECT * FROM Rides WHERE rideID = ?", [
            req.params.id,
        ]);
        if (row) {
            logger.info("Get single ride, rideID:." + req.params.id);
            return res.status(200).send(row);
        } else {
            logger.error("RIDES_NOT_FOUND_ERROR rideID:" + req.params.id);
            return res.status(404).send({
                error_code: "RIDES_NOT_FOUND_ERROR",
                message: "Could not find any rides",
            });
        }
    },

    store: async (req, res) => {
        const startLatitude = Number(req.body.start_lat);
        const startLongitude = Number(req.body.start_long);
        const endLatitude = Number(req.body.end_lat);
        const endLongitude = Number(req.body.end_long);
        const riderName = req.body.rider_name;
        const driverName = req.body.driver_name;
        const driverVehicle = req.body.driver_vehicle;

        if (
            startLatitude < -90 ||
            startLatitude > 90 ||
            startLongitude < -180 ||
            startLongitude > 180
        ) {
            logger.error(
                "VALIDATION_ERROR: Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively"
            );
            return res.status(400).send({
                error_code: "VALIDATION_ERROR",
                message:
                    "Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
            });
        }

        if (
            endLatitude < -90 ||
            endLatitude > 90 ||
            endLongitude < -180 ||
            endLongitude > 180
        ) {
            logger.error(
                "VALIDATION_ERROR: End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively"
            );
            return res.status(400).send({
                error_code: "VALIDATION_ERROR",
                message:
                    "End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
            });
        }

        if (typeof riderName !== "string" || riderName.length < 1) {
            logger.error(
                "VALIDATION_ERROR: Rider name must be a non empty string"
            );
            return res.status(400).send({
                error_code: "VALIDATION_ERROR",
                message: "Rider name must be a non empty string",
            });
        }

        if (typeof driverName !== "string" || driverName.length < 1) {
            logger.error(
                "VALIDATION_ERROR: Driver name must be a non empty string"
            );
            return res.status(400).send({
                error_code: "VALIDATION_ERROR",
                message: "Driver name must be a non empty string",
            });
        }

        if (typeof driverVehicle !== "string" || driverVehicle.length < 1) {
            logger.error(
                "VALIDATION_ERROR: Driver vehicle must be a non empty string"
            );
            return res.status(400).send({
                error_code: "VALIDATION_ERROR",
                message: "Driver vehicle must be a non empty string",
            });
        }

        var values = [
            req.body.start_lat,
            req.body.start_long,
            req.body.end_lat,
            req.body.end_long,
            req.body.rider_name,
            req.body.driver_name,
            req.body.driver_vehicle,
        ];

        const result = await db.run(
            "INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES (?, ?, ?, ?, ?, ?, ?)",
            values
        );
        logger.info("Created new ride, rideID: " + result.lastID);
        const row = await db.get(
            "SELECT * FROM Rides WHERE rideID = ?",
            result.lastID
        );
        return res.status(201).send(row);
    },
};
