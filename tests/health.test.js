"use strict";

const request = require("supertest");
const app = require("../app");

describe("Health check", () => {
    it("should return health", (done) => {
        request(app)
            .get("/health")
            .expect("Content-Type", /text/)
            .expect(200, done);
    });
});
