const logger = require("../../../component/logger");

module.exports = {
    index: function (req, res) {
        logger.info("Health check.");
        res.status(200).send("Healthy");
    },
};
