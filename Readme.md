# A Simple Ride Hailing RESTful APIs

This project contains APIs for managing users' rides.

## Requirements

- Node.js (>v8.6.0 and <=v10.0.0>)
- npm (normally comes with Node.js)

## Setup

- Install dependencies:
```
npm install
```
- To run unit testing:
```
npm test
```
- To run the app in development:
```
npm start
```


## Usage
Here are the list of API that can be used in this project:

#### 1. GET /health
This is an entry point to check whether the server is running correctly.

**Successful Outcomes**

| Code | Description |
| ------ | ------ |
| 200 | Healthy |

**Exceptional Outcomes**

| Code | Description |
| ------ | ------ |
| 500 | An unexpected server-side error occurred |


#### 2. POST /rides
Create a ride.

**Successful Outcomes**

| Code | Description |
| ------ | ------ |
| 201 | The ride has been created |

**Exceptional Outcomes**

| Code | Description |
| ------ | ------ |
| 400 | A required input argument is not present |
| 500 | An unexpected server-side error occurred |

**Examples**

```
POST /rides
Content-Type: application/json
{
    "start_lat":0,
    "start_long":0,
    "end_lat":0,
    "end_long":0,
    "rider_name":"Rider",
    "driver_name":"Driver",
    "driver_vehicle":"Vehicle"
}
```
```
201 Created
[
    {
        "rideID": 2,
        "startLat": 0,
        "startLong": 0,
        "endLat": 0,
        "endLong": 0,
        "riderName": "Rider",
        "driverName": "Driver",
        "driverVehicle": "Vehicle",
        "created": "2020-10-21 16:11:37"
    }
]
```

#### 3. GET /rides
List all rides.

**Successful Outcomes**

| Code | Description |
| ------ | ------ |
| 200 | Success |

**Exceptional Outcomes**

| Code | Description |
| ------ | ------ |
| 500 | An unexpected server-side error occurred |

##### Limit Parameter
Use parameter `limit` to set maximum entities per page. By default, it will return pages of 10 entities.

##### Page Parameter
The `page` parameter indicates the page number, the page being the one requested. By default, this query parameter is equal to 1.

**Examples**

```
GET /rides
Accept: application/json
```
```
200 OK
[
    {
        "rideID": 1,
        "startLat": 0,
        "startLong": 100,
        "endLat": 0,
        "endLong": 100,
        "riderName": "Rider 1",
        "driverName": "Driver 1",
        "driverVehicle": "Vehicle 1",
        "created": "2020-10-21 14:58:10"
    },
    {
        "rideID": 2,
        "startLat": 90,
        "startLong": 120,
        "endLat": 90,
        "endLong": 120,
        "riderName": "Rider 2",
        "driverName": "Driver 2",
        "driverVehicle": "Vehicle 2",
        "created": "2020-10-21 14:58:10"
    }
]
```

```
GET /rides?limit=1&page=2
Accept: application/json
```
```
200 OK
[
    {
        "rideID": 2,
        "startLat": 90,
        "startLong": 120,
        "endLat": 90,
        "endLong": 120,
        "riderName": "Rider 2",
        "driverName": "Driver 2",
        "driverVehicle": "Vehicle 2",
        "created": "2020-10-21 14:58:10"
    }
]
```

#### 4. GET /rides/{}
Detail about a ride.

**Successful Outcomes**

| Code | Description |
| ------ | ------ |
| 200 | Success |

**Exceptional Outcomes**

| Code | Description |
| ------ | ------ |
| 404 | The ride does not exist |
| 500 | An unexpected server-side error occurred |

**Examples**

```
GET /rides/1
Accept: application/json
```
```
200 OK
[
    {
        "rideID": 1,
        "startLat": 0,
        "startLong": 100,
        "endLat": 0,
        "endLong": 100,
        "riderName": "Rider 1",
        "driverName": "Driver 1",
        "driverVehicle": "Vehicle 1",
        "created": "2020-10-21 14:58:10"
    }
]
```