const sqlite3 = require("sqlite3").verbose();
const buildSchemas = require("./src/schemas");

// const db = new sqlite3.Database(":memory:");

var _db;
module.exports = {
    connect: function (cb) {
        if (_db) return cb(_db);

        new sqlite3.Database(":memory:", function () {
            _db = this;
            _db.serialize(() => {
                buildSchemas(_db);
                cb(_db);
            });
        });
    },

    query: function (qry, params) {
        return new Promise((resolve, reject) => {
            this.connect(function (db) {
                db.all(qry, params, function (err, results) {
                    if (err) reject(err);
                    resolve(results);
                });
            });
        });
    },

    get: function (qry, params) {
        return new Promise((resolve, reject) => {
            this.connect(function (db) {
                db.get(qry, params, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            });
        });
    },

    run: function (qry, params) {
        return new Promise((resolve, reject) => {
            this.connect(function (db) {
                db.run(qry, params, function (err) {
                    if (err) reject(err);
                    resolve(this);
                });
            });
        });
    },

    close: function(cb){
        this.connect(function (db) {
            db.close(function (err) {
                if (err) cb(err);
                cb(null);
            });
        });
    }
};
// const buildSchemas = require("./src/schemas");

// module.exports = db;
